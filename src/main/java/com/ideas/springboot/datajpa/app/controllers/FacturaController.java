package com.ideas.springboot.datajpa.app.controllers;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ideas.springboot.datajpa.app.models.entity.Cliente;
import com.ideas.springboot.datajpa.app.models.entity.Factura;
import com.ideas.springboot.datajpa.app.models.entity.ItemFactura;
import com.ideas.springboot.datajpa.app.models.entity.Producto;
import com.ideas.springboot.datajpa.app.models.service.IClienteService;

@Controller
/*
 * Con la anotación @RequestMapping mapeamos el controlador para que tenga una
 * url base principal, por lo tanto, si queremos acceder a una acción, tenemos
 * que escribir primero la url del controlador, que sería la de primer nivel y
 * luego se escribe la ruta o el path de la acción, por ejemplo:
 * http://localhost:8080/factura/form/1
 * 
 */
@RequestMapping("/factura")
/*
 * Todos los métodos del controlador son para ROLA_ADMIN, entonces, pare evitar
 * tener que anotar cada método, simplemente colocamos la anotación al inicio de
 * la clase, asi se aplicará la anotación a todos los métodos.
 */
@Secured("ROLE_ADMIN")
@SessionAttributes("factura")
public class FacturaController {
	@Autowired
	private IClienteService clienteService;

	@Autowired
	private MessageSource messageSource;

	private final Logger log = LoggerFactory.getLogger(getClass());

	@GetMapping("/ver/{id}")
	public String ver(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash, Locale locale) {
		Factura factura = clienteService.fetchByIdWithClienteWithItemFacturaWithProducto(id);// clienteService.findFacturaById(id);

		if (factura == null) {
			flash.addFlashAttribute("error", messageSource.getMessage("text.factura.flash.db.error", null, locale));
			return "redirect:/listar";
		}

		model.addAttribute("factura", factura);
		model.addAttribute("titulo", String.format(messageSource.getMessage("text.factura.ver.titulo", null, locale),
				factura.getDescripcion()));
		return "factura/ver";
	}

	@GetMapping("/form/{clienteId}")
	public String crear(@PathVariable(value = "clienteId") Long clienteId, Map<String, Object> model,
			RedirectAttributes flash, Locale locale) {

		Cliente cliente = clienteService.findOne(clienteId);
		if (cliente == null) {
			flash.addAttribute("error", messageSource.getMessage("text.cliente.flash.db.error", null, locale));
			return "redirect:/listar";
		}

		Factura factura = new Factura();
		factura.setCliente(cliente);

		model.put("factura", factura);
		model.put("titulo", messageSource.getMessage("text.factura.form.titulo", null, locale));
		return "factura/form";
	}

	/*
	 * Se va a generar una salida del tipo 'application/json', eso lo especificamos
	 * con el parametro 'produces'
	 */
	/*
	 * La anotación @ResponseBody lo que hace es suprimir el cargar una vista de
	 * thymeleaf, y en lugar de eso va a tomar el resultado de lo que se está
	 * retornando convertido a json, y eso lo va a registrar o poblar dentro del
	 * body de la respuesta. (TRANSFORMA LA SALIDA EN JSON, Y LA GUARDA DENTRO DEL
	 * RESPONSE)
	 */
	@GetMapping(value = "cargar-productos/{term}", produces = { "application/json" })
	public @ResponseBody List<Producto> cargarProducto(@PathVariable("term") String term) {
		return clienteService.findByNombre(term);
	}

	@PostMapping(value = "/form")
	public String guardar(@Valid Factura factura, BindingResult result, Model model,
			@RequestParam(value = "item_id[]", required = false) Long[] itemId,
			@RequestParam(value = "cantidad[]", required = false) Integer[] cantidad, RedirectAttributes flash,
			SessionStatus status, Locale locale) {

		if (result.hasErrors()) {
			model.addAttribute("titulo", messageSource.getMessage("text.factura.form.titulo", null, locale));
			return "factura/form";
		}

		if (itemId == null || itemId.length == 0) {
			model.addAttribute("titulo", messageSource.getMessage("text.factura.form.titulo", null, locale));
			model.addAttribute("error", messageSource.getMessage("text.factura.flash.lineas.error", null, locale));
			return "factura/form";
		}

		for (int i = 0; i < itemId.length; i++) {
			Producto producto = clienteService.findProductoById(itemId[i]);

			ItemFactura linea = new ItemFactura();
			linea.setCantidad(cantidad[i]);
			linea.setProducto(producto);

			factura.addItemFactura(linea);

			log.info("ID: " + itemId[i].toString() + ", cantidad: " + cantidad[i].toString());
		}

		clienteService.saveFactura(factura);
		flash.addFlashAttribute("success", messageSource.getMessage("text.factura.flash.crear.success", null, locale));
		status.setComplete();

		return "redirect:/ver/" + factura.getCliente().getId();
	}

	@GetMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Locale locale) {
		Factura factura = clienteService.findFacturaById(id);

		if (factura != null) {
			clienteService.deleteFactura(id);
			flash.addFlashAttribute("success",
					messageSource.getMessage("text.factura.flash.eliminar.success", null, locale));
			return "redirect:/ver/" + factura.getCliente().getId();
		}

		flash.addFlashAttribute("error", messageSource.getMessage("text.factura.flash.db.error", null, locale));
		return "redirect:/listar";
	}
}
