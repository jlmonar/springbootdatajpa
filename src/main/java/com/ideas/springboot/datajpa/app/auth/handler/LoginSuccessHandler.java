package com.ideas.springboot.datajpa.app.auth.handler;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.SessionFlashMapManager;

/*
 * Anotamos la clase con Component para registrarla como un Bean de Spring, se guarda en el
 * contenedor dentro del contexto de Spring, esto me permite poder inyectar la clase.
 */
@Component
public class LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private LocaleResolver localeResolver;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		/*
		 * Administrador de Map para los mensajes flash. Estamos usando esta forma ya
		 * que acá no se puede inyectar como argumento en el método el RedirectAttribute
		 * como se puede hacer en el controlador, entonces tenemos que hacerlo de esta
		 * forma, pero básicamente se llega a lo mismo, a guardar los mensajes en flash.
		 */
		SessionFlashMapManager flashMapManager = new SessionFlashMapManager();
		FlashMap flashMap = new FlashMap();

		Locale locale = localeResolver.resolveLocale(request);
		String mensaje = String.format(messageSource.getMessage("text.login.success", null, locale),
				authentication.getName());

		flashMap.put("success", mensaje);

		flashMapManager.saveOutputFlashMap(flashMap, request, response);

		if (authentication != null) {
			logger.info("El usuario " + authentication.getName() + " ha iniciado sesión con éxito!");
		}

		super.onAuthenticationSuccess(request, response, authentication);
	}

}
