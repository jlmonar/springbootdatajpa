package com.ideas.springboot.datajpa.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ideas.springboot.datajpa.app.models.service.IClienteService;
import com.ideas.springboot.datajpa.app.view.xml.ClienteList;

/*
 * Este es un controlador especifico y dedicado 100% a un API Rest, para lo cual se hace
 * uso de la anotación @RestController, la cual está combinada por dos anotaciones, el
 * estereotipo @Controller y @ResponseBody, por lo tanto todos los métodos de este
 * controlador van a ser @ResponseBody
 */
@RestController
@RequestMapping("/api/clientes")
public class ClienteRestController {
	@Autowired
	private IClienteService clienteService;

	@GetMapping(value = "/listar")
	public ClienteList listar() {
		/*
		 * Cambiamos el tipo de retorno a ClientList en caso de que queramos que el
		 * método retorne tanto 'xml' o 'json', pero si queremos solo formato 'json'
		 * podemos omitir la clase wrapper ClientList y retornar simplemente un
		 * List<Cliente>
		 */
		return new ClienteList(clienteService.findAll());
	}
}
