package com.ideas.springboot.datajpa.app.view.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.ideas.springboot.datajpa.app.models.entity.Cliente;

/*
 * Esta sería la clase wrapper que envuelve nuestra lista de clientes. Por lo tanto, si queremos
 * convertir el listado de clientes debemos tener una clase ClienteList que envuelva nuestro objeto 
 * List o Collection dentrode esta clase, se le llama wrapper (Envoltura en español).
 * Esta clase va a representar el Xml root, la raiz del documento XML.
 */
/*
 * Utilizamos la anotación @XmlRootElement para indicar que esta es la clase root XML. Le podemos asignar
 * un nombre, si no se le asigna va a tomar por defecto el nombre de la clase, pero la idea aqui es que
 * la raiz tenga el nombre 'clientes' (es decir, habran varios elementos del tipo cliente).
 */
@XmlRootElement(name = "clientes")
public class ClienteList {
	/*
	 * Además debemos indicar, cual es el atributo que va a ser un elemento xml por
	 * cada cliente. Vamos a tener un elemento cliente por cada cliente que haya en
	 * la lista.
	 */
	@XmlElement(name = "cliente")
	public List<Cliente> clientes;

	public ClienteList() {
	}

	public ClienteList(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}
}
