package com.ideas.springboot.datajpa.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.validation.constraints.NotEmpty;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "facturas")
public class Factura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	private String descripcion;

	private String observacion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_at")
	private Date createAt;

	/*
	 * Muchas facturas a un cliente. Many se refiere a la clase en la cual estamos.
	 */
	/*
	 * Con LAZY hacemos carga perezosa (Es la forma más recomendada). EAGER trae
	 * todo de una sola consulta, por ejemplo, si hacemos la consulta a una factura,
	 * va a realizar también la consulta del cliente, si hacemos la consulta a una
	 * lista de facturas, entonces por cada factura va a traer a su cliente y carga
	 * demasiado a la base de datos. Lo ideal es utilizzar carga LAZY, de este modo
	 * las consultas se realizan de forma perezosa a medida que se van invocando los
	 * metodos, por ejemplo, cuando se invoca el método getCliente(), ahi recién se
	 * realiza la consulta del cliente de la factura.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	/*
	 * Usamos JsonBackReference aquí porque es la contraparte de la relación (entre
	 * Cliente y Factura) que no queremos mostrar o serializar en el Json, por lo
	 * tanto esta anotación hace que se omita de la serialización ya que es la parte
	 * posterior de la relación.
	 */
	@JsonBackReference
	private Cliente cliente;

	/*
	 * Si se elimina una factura, se deben eliminar todos sus elementos hijos, por
	 * eso usamos aqui el cascade, en la clase padre
	 */
	/*
	 * ItemFactura no tiene la relación con factura, ya que no la necesita, en
	 * ningún momento vamos a utilizar un item para obtener la factura, pero si
	 * necesitamos que la factura obtenga a sus hijos para mostrar el detalle,es
	 * decir, las lineas de la factura. Esta es una relacion OneToMany porque es una
	 * relacion de 1 factura hacia muchos items
	 */
	/*
	 * Aquí tenemos que indicar, ya que la relación va a ser en un solo sentido,
	 * cual es la llave foránea que va a relacionar la tabla Factura con
	 * ItemFactura, para eso usamos la anotacion @JoinColumn la cual hace que la
	 * tabla facturas_items tenga la llave foránea 'factura_id'. Por lo tanto,
	 * estamos en una relacion UNIDIRECCIONAL.
	 */
	/*
	 * opphanRemoval es para borrar todos los registros huérfanos que quedaron en
	 * una tabla cuando se elimina el padre, por ejemplo si tenemos la tabla
	 * items_factura que están relacionado a una factura, pero la factura se
	 * eliminó, entonces también se eliminan sus hijos. pero de todas formas con
	 * CascadeType.ALL lo hace de forma automática, se lo colocó más que nada para
	 * complementar.
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "factura_id")
	private List<ItemFactura> items;

	public Factura() {
		this.items = new ArrayList<ItemFactura>();
	}

	@PrePersist
	public void prePersist() {
		createAt = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	/*
	 * El cliente está relacionado con la factura, pero la factura tambien está
	 * relacionado con el cliente (como se puede ver aquí), por lo tanto cuando se
	 * vaya a generar una respuesta xml de una lista de clientes, va a convertir el
	 * listado de clientes a xml junto a sus facturas, pero a su vez, las facturas
	 * también tiene una relación con cliente, por lo tanto factura va a serializar
	 * nuevamente a cliente, y cliente nuevamente a factura y se transforma en un
	 * loop infinito sin salida. Para resolver esto usamos la siguiente
	 * anotación @XmlTransient, la palabra transient significa que cuando se
	 * serializa, no va a llamar a este método. Aqui lo se se esta haciendo es
	 * bloquear el acceso para que no vaya en la dirección del cliente, sino siempre
	 * adelante, una sola dirección.
	 */
	@XmlTransient
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<ItemFactura> getItems() {
		return items;
	}

	public void setItems(List<ItemFactura> items) {
		this.items = items;
	}

	public void addItemFactura(ItemFactura item) {
		this.items.add(item);
	}

	public Double getTotal() {
		Double total = 0.0;

		int size = items.size();

		for (int i = 0; i < size; i++) {
			total += items.get(i).calcularimporte();
		}
		return total;
	}
}
