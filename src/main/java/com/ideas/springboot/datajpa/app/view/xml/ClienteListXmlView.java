package com.ideas.springboot.datajpa.app.view.xml;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.xml.MarshallingView;

import com.ideas.springboot.datajpa.app.models.entity.Cliente;

@Component("listar.xml")
public class ClienteListXmlView extends MarshallingView {

	@Autowired
	public ClienteListXmlView(Jaxb2Marshaller marshaller) {
		super(marshaller);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		/*
		 * Titulo y page no nos interesa, pues lo que realmente nos interesa es exportar
		 * a xml los clientes, por lo tanto, removemos del modelo tanto titulo como
		 * page.
		 */
		model.remove("titulo");
		model.remove("page");

		/*
		 * Los clientes también removemos del modelo, pero antes los guardamos en una
		 * variable, ya que debemos envolverlos dentro de la clase wrapper, es decir,
		 * debe estar embebido dentro de la clase ClienteList, para ello hacemos las
		 * siguientes transformaciones.
		 */
		//Page<Cliente> clientes = (Page<Cliente>) model.get("clientes");
		List<Cliente> clientes = (List<Cliente>) model.get("clientes");

		model.remove("clientes");

		/*
		 * Colocamos la lista de clientes dentro de la clase wrapper ClienteList.
		 */
		model.put("clienteList", new ClienteList(clientes));
		//model.put("clienteList", new ClienteList(clientes.getContent()));

		super.renderMergedOutputModel(model, request, response);
	}

}
