package com.ideas.springboot.datajpa.app.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/*
 * Este controlador se va a encargar de obtener a través de un request , obtener la cabacera,}
 * el header y a través del header vamos a obtener la última url en la que estabamos
 */
@Controller
public class LocaleController {

	/*
	 * Método handler que se encarga de manejar una petición o request.
	 */
	@GetMapping("/locale")
	public String locale(HttpServletRequest request) {
		/*
		 * referer lo que nos entraga es al referencia de la última url, el link de
		 * nuestra última página.
		 */
		String ultimaUrl = request.getHeader("referer");

		return "redirect:".concat(ultimaUrl);

	}

}
