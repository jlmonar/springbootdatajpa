package com.ideas.springboot.datajpa.app.view.json;

import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Component("listar.json")
public class ClientListJsonView extends MappingJackson2JsonView {

	/*
	 * Usamos este override para agregar o eliminar algunos elementos del model.
	 */
	@Override
	protected Object filterModel(Map<String, Object> model) {
		/*
		 * Podriamos obviar el hacer override de esta función, ya que el model que
		 * estamos recibiendo no va a contener los elementos 'titulo' ni 'page' según la
		 * condición en ClienteController cuando queremos ver la vista en formato json,
		 * simplemente hacemos este paso de override por cuestiones didácticas nada más.
		 */
		model.remove("titulo");
		model.remove("page");
		return super.filterModel(model);
	}
}
