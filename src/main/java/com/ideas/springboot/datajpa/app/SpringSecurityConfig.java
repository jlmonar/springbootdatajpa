package com.ideas.springboot.datajpa.app;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ideas.springboot.datajpa.app.auth.handler.LoginSuccessHandler;
import com.ideas.springboot.datajpa.app.models.service.JpaUserDetailsService;

/*
 * Esta anotación es importante, ya que aqui habilitamos el uso de las anotaciones
 * @Secured y @PreAuthorize para dar seguridades.
 */
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private LoginSuccessHandler successHandler;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private JpaUserDetailsService userDetailsService;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	/*
	 * Spring Security por detrás de escena lo que hace es ejecutar un filtro, un
	 * interceptor antes de cargar cualquier página, cualquier ruta de nuestros
	 * controladores, va a interceptar antes y va a validar que el usuario tenga
	 * permisos, si no los tiene, se mostrará la ventana de acceso denegado o
	 * redirigir a la página de login.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/*
		 * Primero vamos a colocar las rutas publicas, que sean de acceso a todo el
		 * mundo, tanto para usuarios logueados, ADMINS y usuarios anónimos. Luego
		 * colocamos las rutas privadas que queremos proteger.
		 */
		/*
		 * En la ruta '/listar**' y '/api/clientes/**' los asteriscos indican que se
		 * puede permitir cualquier url que comience con '/listar' o '/api/clientes/' y
		 * lo que venga después se va a permitir, por lo tanto, rutas como /listar-rest
		 * y /api/clientes/listar van a estar permitidas.
		 */
		http.authorizeRequests()
				.antMatchers("/", "/css/**", "/js/**", "/images/**", "/listar**", "/api/clientes/**", "/locale")
				.permitAll()
				/* .antMatchers("/ver/**").hasAnyRole("USER") */// Reemplazamos todas autorizaciones con las anotaciones
				/* .antMatchers("/uploads/**").hasAnyRole("USER") */
				/* .antMatchers("/form/**").hasAnyRole("ADMIN") */
				/* .antMatchers("/delete/**").hasAnyRole("ADMIN") */
				/* .antMatchers("/factura/**").hasAnyRole("ADMIN") */
				.anyRequest().authenticated().and().formLogin()
				/*
				 * Si queremos que luego del loguearnos exitosamente nos redirija a la página
				 * que estabamos previamente, entonces deshabilitamos el successHandler, ya que
				 * este hace que al haber un logueo exitoso me redirija a la página de inicio.
				 */
				.successHandler(successHandler).loginPage("/login").permitAll().and().logout().permitAll().and()
				.exceptionHandling().accessDeniedPage("/error_403");
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder build) throws Exception {
		/*
		 * Implementación de Spring Security con JPA.
		 */
		build.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);

		/*
		 * Implementación de Spring Security con JDBC Authentication. Aquí configuramos
		 * la consulta SQL para la autenticación, podemos customizar nustra consulta de
		 * inicio de sesión la cual seria una consulta sql nativa (nada que ver con
		 * JPA), usersByUsernameQuery usamos para obtener el usuario y
		 * authoritiesByUsernameQuery para obtener los roles.
		 */
		/*
		 * build.jdbcAuthentication() .dataSource(dataSource)
		 * .passwordEncoder(passwordEncoder)
		 * .usersByUsernameQuery("select username, password, enabled from users where username=?"
		 * )
		 * .authoritiesByUsernameQuery("select u.username, a.authority from authorities a inner join users u on (a.user_id = u.id) where u.username=?"
		 * );
		 */

		/*
		 * PasswordEncoder encoder =
		 * PasswordEncoderFactories.createDelegatingPasswordEncoder(); UserBuilder users
		 * = User.builder().passwordEncoder(encoder::encode);
		 */
		/*
		 * Vamos a atilizar un sistema de autentificación en memoria como un repositorio
		 * donde vamos a crear los usuarios con sus credenciales y roles.
		 */
		/*
		 * build.inMemoryAuthentication()
		 * .withUser(users.username("admin").password("12345").roles("ADMIN", "USER"))
		 * .withUser(users.username("jose").password("12345").roles("USER"));
		 */
	}
}
